package com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.Length;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "TB_TOPICS")
public class TopicModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID topicId;

    @Length(min = 10, max = 250)
    private String topicTitle;

    private String topicSubTitle;

    @ManyToOne()
    @JoinColumn(name = "idUser", insertable = false, updatable = false)
    private UserModel userModel;

    private UUID idUser;

    @CreationTimestamp
    private LocalDateTime createdAt;

    public void setTopicTitle(String topicTitle) throws Exception {
        if (topicTitle.length() > 200) {
            throw new Exception("The field should have 200 characters");
        }

        this.topicTitle = topicTitle;
    }


}
