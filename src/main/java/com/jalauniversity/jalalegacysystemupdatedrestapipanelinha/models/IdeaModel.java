package com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "TB_IDEAS")
public class IdeaModel implements Serializable {

    private final static long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID ideaId;

    private String ideaTitle;

    private String ideaParagraph;

    @ManyToOne()
    @JoinColumn(name = "topicId", insertable = false, updatable = false)
    private TopicModel topicModel;

    private UUID topicId;

    @CreationTimestamp
    private LocalDateTime createdAt;
}
