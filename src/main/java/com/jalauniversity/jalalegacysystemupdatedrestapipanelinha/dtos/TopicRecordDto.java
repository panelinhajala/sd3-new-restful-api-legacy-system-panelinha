package com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.dtos;

import jakarta.validation.constraints.NotBlank;

public record TopicRecordDto(@NotBlank String topicTitle, @NotBlank String topicSubTitle) {

}
