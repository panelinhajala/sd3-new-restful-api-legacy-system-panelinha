package com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.dtos;

import jakarta.validation.constraints.NotNull;

public record UserRecordDto(@NotNull String userName, @NotNull String loginUser, @NotNull String passwordUser) {

}
