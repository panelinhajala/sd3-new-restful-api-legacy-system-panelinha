package com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.dtos;

import jakarta.validation.constraints.NotNull;

public record UserLoginRecordDto(@NotNull String loginUser, @NotNull String passwordUser) {

}
