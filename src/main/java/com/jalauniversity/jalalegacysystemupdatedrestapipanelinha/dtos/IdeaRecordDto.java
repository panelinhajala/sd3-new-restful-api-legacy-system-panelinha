package com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.dtos;

import jakarta.validation.constraints.NotNull;

public record IdeaRecordDto(@NotNull String ideaTitle, @NotNull String ideaParagraph) {
}
