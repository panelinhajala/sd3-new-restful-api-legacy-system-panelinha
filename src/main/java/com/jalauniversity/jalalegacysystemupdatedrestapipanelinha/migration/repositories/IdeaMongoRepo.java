package com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.migration.repositories;

import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.migration.model.IdeaModelMongo;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface IdeaMongoRepo extends MongoRepository<IdeaModelMongo, String> {
}
