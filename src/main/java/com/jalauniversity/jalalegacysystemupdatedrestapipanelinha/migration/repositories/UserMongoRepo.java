package com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.migration.repositories;

import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.migration.model.UserModelMongo;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserMongoRepo extends MongoRepository<UserModelMongo, String> {
}
