package com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.migration.repositories;

import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.migration.model.TopicModelMongo;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TopicMongoRepo extends MongoRepository<TopicModelMongo, String> {
}
