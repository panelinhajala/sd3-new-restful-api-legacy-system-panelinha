package com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.migration.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;


@Data
@AllArgsConstructor
@Document(collection = "topic")
public class TopicModelMongo{

    @MongoId
    private String topicId;

    @Length(min = 10, max = 250)
    private String topicTitle;

    private String topicSubTitle;

    private String idUser;

    @CreationTimestamp
    private LocalDateTime createdAt;

    public void setTopicTitle(String topicTitle) throws Exception {
        if (topicTitle.length() > 200) {
            throw new Exception("The field should have 200 characters");
        }

        this.topicTitle = topicTitle;
    }


}
