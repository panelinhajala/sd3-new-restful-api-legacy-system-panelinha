package com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.migration.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.time.LocalDateTime;


@Data
@AllArgsConstructor
@Document(collection = "idea")
public class IdeaModelMongo {

    @MongoId
    private String ideaId;

    @Field(name = "TITLE_IDEA")
    private String ideaTitle;

    @Field(name = "PARAGRAPH_IDEA")
    private String ideaParagraph;

    @Field(name = "TOPIC_ID")
    private String topicId;

    @CreationTimestamp
    @Field(name = "DT_CREATED_AT")
    private LocalDateTime createdAt;
}
