package com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.migration.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.MongoId;


@Data
@ToString
@AllArgsConstructor
@Document(collection = "user")
public class UserModelMongo{


    @MongoId
    private String idUser;

    @Field(name = "USER_NAME")
    private String userName;

    @Field(name = "USER_LOGIN")
    private String loginUser;

    @Length(max = 255)
    @Field(name = "USER_PASSWORD")
    private String passwordUser;

}
