package com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.migration;

import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.migration.model.IdeaModelMongo;
import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.migration.model.TopicModelMongo;
import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.migration.model.UserModelMongo;
import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.migration.repositories.IdeaMongoRepo;
import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.migration.repositories.TopicMongoRepo;
import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.migration.repositories.UserMongoRepo;
import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.models.IdeaModel;
import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.models.TopicModel;
import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.models.UserModel;
import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.repositories.IdeaRepository;
import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.repositories.TopicRepository;
import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MigrationService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TopicRepository topicRepository;
    @Autowired
    private IdeaRepository ideaRepository;
    @Autowired
    private TopicMongoRepo topicMongoRepo;
    @Autowired
    private IdeaMongoRepo ideaMongoRepo;
    @Autowired
    private UserMongoRepo userMongoRepo;

    public void migrate() {
        List<UserModel> userElements = userRepository.findAll();
        for (UserModel usertElement : userElements) {
            UserModelMongo userMongo = new UserModelMongo(
                    usertElement.getIdUser().toString(),
                    usertElement.getUserName(),
                    usertElement.getLoginUser(),
                    usertElement.getPasswordUser());

            userMongoRepo.save(userMongo);
        }
        List<IdeaModel> ideaElements = ideaRepository.findAll();
        for (IdeaModel ideaElement : ideaElements) {
            IdeaModelMongo ideaMongo = new IdeaModelMongo(
                    ideaElement.getIdeaId().toString(),
                    ideaElement.getIdeaTitle(),
                    ideaElement.getIdeaParagraph(),
                    ideaElement.getTopicId().toString(),
                    ideaElement.getCreatedAt());

            ideaMongoRepo.save(ideaMongo);
        }
        List<TopicModel> topicElements = topicRepository.findAll();
        for (TopicModel topicElement : topicElements) {
            TopicModelMongo topicMongo = new TopicModelMongo(
                    topicElement.getTopicId().toString(),
                    topicElement.getTopicTitle(),
                    topicElement.getTopicSubTitle(),
                    topicElement.getIdUser().toString(),
                    topicElement.getCreatedAt());

            topicMongoRepo.save(topicMongo);
        }
    }
}

