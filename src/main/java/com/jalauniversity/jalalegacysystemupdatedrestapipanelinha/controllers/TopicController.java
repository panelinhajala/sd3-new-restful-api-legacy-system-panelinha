package com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.controllers;

import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.dtos.TopicRecordDto;
import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.models.TopicModel;
import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.services.TopicService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/topics")
public class TopicController {

    @Autowired
    TopicService topicService;


    @PostMapping("/create")
    public ResponseEntity<TopicModel> createTopic(@RequestBody TopicRecordDto topicRecordDto, HttpServletRequest request) throws Exception {

        TopicModel topicCreated = topicService.createTopic(topicRecordDto);

        return ResponseEntity.status(HttpStatus.CREATED).body(topicCreated);
    }

    @GetMapping("/mytopics")
    public ResponseEntity<List<TopicModel>> listTopic(HttpServletRequest request) {
         var idUser =  request.getAttribute("idUser");
        List<TopicModel> topicList = topicService.listTopic(UUID.fromString(idUser.toString()));

        return ResponseEntity.status(HttpStatus.OK).body(topicList);
    }

    @GetMapping
    public ResponseEntity<List<TopicModel>> listAllTopics() {
        List<TopicModel> allTopics = topicService.listAllTopics();

        return ResponseEntity.status(HttpStatus.OK).body(allTopics);
    }

}
