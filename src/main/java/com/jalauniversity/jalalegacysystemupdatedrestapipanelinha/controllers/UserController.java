package com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.controllers;

import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.dtos.UserRecordDto;
import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.models.UserModel;
import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.repositories.UserRepository;
import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.services.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;


    @PostMapping("/create")
    public ResponseEntity<UserModel> saveUser(@Valid @RequestBody UserRecordDto userRecordDto) {
        UserModel savedUser = userService.saveUser(userRecordDto);
        System.out.println(savedUser);
        return ResponseEntity.status(HttpStatus.OK).body(savedUser);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable(value = "id") UUID id, @RequestBody UserRecordDto userRecordDto) {
        Optional<UserModel> updatedUser = userService.updateUser(id, userRecordDto);
        if (updatedUser.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(updatedUser.get());
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found");
        }
    }

    @GetMapping("/find")
    public ResponseEntity<List<UserModel>> getAllUsers() {
        List<UserModel> users = userService.getAllUsers();
        return ResponseEntity.ok(users);
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<UserModel> getUserById(@PathVariable UUID id) {
        Optional<UserModel> userOp = userService.getUserById(id);
        if (userOp.isPresent()) {
            return ResponseEntity.ok(userOp.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable UUID id) {
        boolean deleted = userService.deleteUser(id);
        if (deleted) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

}

