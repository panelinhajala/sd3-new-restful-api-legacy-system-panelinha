package com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.controllers;

import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.dtos.UserLoginRecordDto;
import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.usecases.AuthUserUserCase;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.naming.AuthenticationException;

@RestController
@RequestMapping("/auth")
public class UserAuthController {

    @Autowired
    AuthUserUserCase authUserUserCase;

    private AuthenticationManager authenticationManager;


    @PostMapping("/user")
    public ResponseEntity<Object> create(@RequestBody @Valid UserLoginRecordDto userLoginRecordDto) throws AuthenticationException {
        try {
            var result = this.authUserUserCase.execute(userLoginRecordDto);
            return ResponseEntity.ok().body(result);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
        }
    }
}
