package com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.controllers;

import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.dtos.IdeaRecordDto;
import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.models.IdeaModel;
import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.services.IdeaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("{id}/ideas")
public class IdeaController {

    @Autowired
    IdeaService ideaService;

        @PostMapping("/create")
        public ResponseEntity<IdeaModel> createIdea(@PathVariable UUID id, @RequestBody IdeaRecordDto dto){
            IdeaModel ideaCreated = ideaService.createIdea(dto, id);
            return ResponseEntity.status(HttpStatus.CREATED).body(ideaCreated);
        }

    @GetMapping
    public ResponseEntity<List<IdeaModel>> listAllIdeas(@PathVariable UUID id){
        List<IdeaModel> allIdeas = ideaService.listAllIdeas(id);
        return ResponseEntity.status(HttpStatus.OK).body(allIdeas);
    }

}

