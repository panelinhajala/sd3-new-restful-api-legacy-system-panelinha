package com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.usecases;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.dtos.UserLoginRecordDto;
import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.naming.AuthenticationException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;

@Service
public class AuthUserUserCase {

    @Value("${security.token.secret}")
    private String secretKey;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;


    public String execute(UserLoginRecordDto userLoginRecordDto) throws AuthenticationException {
        var user =
                this.userRepository.findByLoginUser(userLoginRecordDto.loginUser()).orElseThrow(() -> {
                    throw new UsernameNotFoundException("user not found");
                });

        var passwordMatches = this.passwordEncoder.matches(userLoginRecordDto.passwordUser(), user.getPasswordUser());

        //Se não for igual -> Erro
        if (!passwordMatches) {
            throw new AuthenticationException();
        }

        //Se for igual -> Gerar token
        Algorithm algorithm = Algorithm.HMAC256(secretKey);
        var token = JWT.create().withIssuer("jalauniversity")
                .withExpiresAt(Instant.now().plus(Duration.ofHours(2)))
                .withSubject(user.getIdUser().toString())
                .sign(algorithm);

        return token;
    }

}
