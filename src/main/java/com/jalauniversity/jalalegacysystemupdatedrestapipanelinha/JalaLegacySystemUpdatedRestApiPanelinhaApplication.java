package com.jalauniversity.jalalegacysystemupdatedrestapipanelinha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JalaLegacySystemUpdatedRestApiPanelinhaApplication {

    public static void main(String[] args) {
        SpringApplication.run(JalaLegacySystemUpdatedRestApiPanelinhaApplication.class, args);
    }

}
