package com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.repositories;

import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.models.IdeaModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface IdeaRepository extends JpaRepository<IdeaModel, UUID> {


    List<IdeaModel> findByTopicId(UUID topicId);
}
