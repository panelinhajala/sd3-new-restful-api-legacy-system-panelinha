package com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.repositories;

import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.models.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<UserModel, UUID> {

    Optional<UserModel> findByLoginUser(String loginUser);

}
