package com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.repositories;

import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.models.TopicModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface TopicRepository extends JpaRepository<TopicModel, UUID> {

    List<TopicModel> findByIdUser(UUID idUser);
}
