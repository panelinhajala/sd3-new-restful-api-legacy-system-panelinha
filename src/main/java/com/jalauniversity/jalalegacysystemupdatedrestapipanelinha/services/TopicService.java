package com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.services;

import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.dtos.TopicRecordDto;
import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.models.TopicModel;
import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.repositories.TopicRepository;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
public class TopicService {

    @Autowired
    TopicRepository topicRepository;

    @Autowired
    HttpServletRequest request;

    public TopicModel createTopic(TopicRecordDto topicRecordDto) throws Exception {
        TopicModel topicModel = new TopicModel();

        topicModel.setTopicTitle(topicRecordDto.topicTitle());
        topicModel.setTopicSubTitle(topicRecordDto.topicSubTitle());
        topicModel.setCreatedAt(LocalDateTime.now());

        var idUser = request.getAttribute("idUser");
        topicModel.setIdUser(UUID.fromString(idUser.toString()));

        return topicRepository.save(topicModel);
    }

    public List<TopicModel> listTopic(UUID idUser) {
        return topicRepository.findByIdUser(idUser);
    }

    public List<TopicModel> listAllTopics() {
        return topicRepository.findAll();
    }

}

