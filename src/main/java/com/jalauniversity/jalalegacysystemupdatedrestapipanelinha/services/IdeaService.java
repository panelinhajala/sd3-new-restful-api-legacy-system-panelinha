package com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.services;

import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.dtos.IdeaRecordDto;
import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.models.IdeaModel;
import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.repositories.IdeaRepository;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
public class IdeaService {

    @Autowired
    IdeaRepository ideaRepository;

    @Autowired
    HttpServletRequest request;

    public IdeaModel createIdea(IdeaRecordDto ideaRecordDto, UUID id) {

        IdeaModel ideaModel = new IdeaModel();

        ideaModel.setIdeaTitle(ideaRecordDto.ideaTitle());
        ideaModel.setIdeaParagraph(ideaRecordDto.ideaParagraph());
        ideaModel.setCreatedAt(LocalDateTime.now());

        ideaModel.setTopicId(id);
       // var id_topic = request.getAttribute("topicId");

      //  ideaModel.setTopicId((UUID) id_topic);

        return ideaRepository.save(ideaModel);

    }

    public List<IdeaModel> listAllIdeas(UUID topicId) {

        return ideaRepository.findByTopicId(topicId);
    }
}
