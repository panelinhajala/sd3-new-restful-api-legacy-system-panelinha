package com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.services;

import at.favre.lib.crypto.bcrypt.BCrypt;
import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.dtos.UserRecordDto;
import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.exceptions.UserFoundException;
import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.models.UserModel;
import com.jalauniversity.jalalegacysystemupdatedrestapipanelinha.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;


    public UserModel saveUser(UserRecordDto userRecordDto) {
        try {
            UserModel userModel = new UserModel();
            userModel.setUserName(userRecordDto.userName());
            userModel.setLoginUser(userRecordDto.loginUser());
            String encodedPassword = passwordEncoder.encode(userRecordDto.passwordUser());
            userModel.setPasswordUser(encodedPassword);

            // Verifica se já existe um usuário com o mesmo loginUser
            if (userRepository.findByLoginUser(userRecordDto.loginUser()).isPresent()) {
                throw new UserFoundException();
            }

            return userRepository.save(userModel);
        } catch (Exception e) {
            // Logar o erro
            e.printStackTrace();
            throw new RuntimeException("Failed to save user.");
        }
    }


    public Optional<UserModel> getUserById(UUID id) {
        return userRepository.findById(id);
    }

    public List<UserModel> getAllUsers() {
        return userRepository.findAll();
    }

    public Optional<UserModel> updateUser(UUID id, UserRecordDto userRecordDto) {
        var userModel = new UserModel();
        Optional<UserModel> userOptional = userRepository.findById(id);
        var passwordHashed = BCrypt.withDefaults().hashToString(12, userModel.getPasswordUser().toCharArray());
        if (userOptional.isPresent()) {
            userModel = userOptional.get();
            if (!userRecordDto.passwordUser().equals(userModel.getPasswordUser())) {
                userModel.setPasswordUser(passwordHashed);
            }
            userModel.setUserName(userRecordDto.userName());
            userModel.setLoginUser(userRecordDto.loginUser());
            return Optional.of(userRepository.save(userModel));
        } else {
            return Optional.empty();
        }
    }

    public boolean deleteUser(UUID id) {
        Optional<UserModel> userOptional = userRepository.findById(id);
        if (userOptional.isPresent()) {
            userRepository.delete(userOptional.get());
            return true;
        } else {
            return false;
        }
    }
}
